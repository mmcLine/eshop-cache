package com.mmc.common.constant;

/**
 * @description: redis前缀
 * @author: mmc
 * @create: 2019-09-30 22:42
 **/

public class RedisPre {

    private static final String PRODUCT_INVENTORY_PRE="product:inventory:";

    /**
     * 商品库存缓存的key
     * @param id
     * @return
     */
    public static String getProductInventoryKey(Integer id){
        return PRODUCT_INVENTORY_PRE+id;
    }

}
