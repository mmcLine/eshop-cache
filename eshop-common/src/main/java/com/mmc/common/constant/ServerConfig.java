package com.mmc.common.constant;

/**
 * @description: 服务器配置
 * @author: mmc
 * @create: 2019-10-29 23:38
 **/

public class ServerConfig {

    public static final String SERVER_URL1="http://192.168.1.12/";
    public static final String SERVER_URL2="http://192.168.1.13/";
    public static final String SERVER_URL3="http://192.168.1.14/";

    public static final String INVENTORY_URL="http://localhost:8081/";
}
