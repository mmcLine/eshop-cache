package com.mmc.common.constant;

/**
 * @description: zookeeper节点名称常量
 * @author: mmc
 * @create: 2019-10-26 22:38
 **/

public class ZkNodeName {

    public  static final String TASKID_LIST ="/taskid-list";

    public static final String HOT_PRODUCT_NODE="/task-hot-product-list-";

    public static final String TASK_ID="/taskid-lock-";

    public static final String TASKID_STATUS_LOCK="/taskid-status-lock-";

    public static final String TASKID_STATUS="/taskid-status-";

    public static final String PRODUCT_INFO_ID="/product-info-cache-";
}
