package com.mmc.common.request;

/**
 * @description: 请求
 * @author: mmc
 * @create: 2019-09-29 23:28
 **/

public interface Request {

    void process();

    Integer id();
}
