package com.mmc.common.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-01 10:50
 **/

@Getter
@Setter
public class BaseResponse {
    private int responseCode = 200;
    private String message;

    public BaseResponse(int responseCode, String message) {
        this.responseCode = responseCode;
        this.message = message;
    }

    public BaseResponse() {
    }



}
