package com.mmc.common.zk;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.CountDownLatch;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-26 16:20
 **/
@Slf4j
public class ZooKeeperSession implements Serializable {

    private static CountDownLatch connectedSemaphore=new CountDownLatch(1);

    private ZooKeeper zooKeeper;

    private String taskListPath = "/taskid-list-lock";



    private ZooKeeperSession(){
        try {
            this.zooKeeper=new ZooKeeper("192.168.1.12:2181,192.168.1.13:2181,192.168.1.14:2181",500000,new ZookeeperWatcher());
            connectedSemaphore.await();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private class ZookeeperWatcher implements Watcher {

        @Override
        public void process(WatchedEvent watchedEvent) {
            if(Event.KeeperState.SyncConnected==watchedEvent.getState()){
                connectedSemaphore.countDown();
            }
        }
    }


    public void create(String path){
        try {
            zooKeeper.create(path,"".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取分布式锁
     */
    public void acquireDistributedLock(){
        int count=0;
        try {
            zooKeeper.create(taskListPath,"".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);

        } catch (Exception e) {
            while (true){
                try {
                    Thread.sleep(1000);
                    zooKeeper.create(taskListPath,"".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);
                    count++;
                    log.info("获取锁次数："+count);
                } catch (Exception e1) {
                    continue;
                }
                break;
            }
        }
        log.info("获取锁成功。。。"+taskListPath);
    }


    /**
     * 获取快速失败的分布式锁
     */
    public boolean acquireFastFailDistributedLock(String path){
        try {
            zooKeeper.create(path,"".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL);
            return true;
        } catch (Exception e) {
            log.error("获取锁{}失败",path);
            return false;
        }
    }

    public void releaseDistributedLock(String path){
        try {
            zooKeeper.delete(path,-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放分布式锁
     */
    public void releaseDistributedLock(){
        try {
            zooKeeper.delete(taskListPath,-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeeperException e) {
            e.printStackTrace();
        }
    }

    public void setNodeData(String path,String data){
        try {
            Stat stat = zooKeeper.exists(path, false);
            if(stat==null){
                zooKeeper.create(path,"".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
            }
            zooKeeper.setData(path,data.getBytes(),-1);
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getNodeData(String path){
        try {
            return new String(zooKeeper.getData(path,false,new Stat()));
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }



    private static class Singleton{

        private static ZooKeeperSession instance=new ZooKeeperSession();

        private static   ZooKeeperSession getInstance(){
            return instance;
        }

    }

    public static ZooKeeperSession getInstance(){
        return Singleton.getInstance();
    }
}
