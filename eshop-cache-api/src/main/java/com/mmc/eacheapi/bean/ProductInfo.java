package com.mmc.eacheapi.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-02 21:43
 **/
@Data
public class ProductInfo implements Serializable {

    private Integer id;

    private String name;

    private BigDecimal price;

    private Long cityId;

    private String cityName;

    private Long brandId;

    //品牌名称
    private String brandName;

    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
}
