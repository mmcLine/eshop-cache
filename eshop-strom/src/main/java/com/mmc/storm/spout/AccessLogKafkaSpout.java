package com.mmc.storm.spout;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @description: 消费kafka数据的Spout
 * @author: mmc
 * @create: 2019-10-24 22:38
 **/
public class AccessLogKafkaSpout extends BaseRichSpout {

    private SpoutOutputCollector spoutOutputCollector;

    private BlockingQueue<String> queue=new ArrayBlockingQueue<>(1000);

    private Logger log=LoggerFactory.getLogger(AccessLogKafkaSpout.class);

    @Override
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.spoutOutputCollector=spoutOutputCollector;
        startKafkaConsumer();
    }

    @Override
    public void nextTuple() {
        if(queue.size()>0){
            try {
                String message=queue.take();
                spoutOutputCollector.emit(new Values(message));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("message"));
    }

    private void startKafkaConsumer(){
        Properties props=new Properties();
        props.put("zookeeper.connect","192.168.1.12:2181,192.168.1.13:2181,192.168.1.14:2181");
        props.put("group.id","eshop-cache-group");
        props.put("zookeeper.session.timeout.ms","4000000");
        props.put("zookeeper.sync.time.ms","1000");
        props.put("auto.commit.interval.ms","1000");
        ConsumerConfig consumerConfig=new ConsumerConfig(props);
        kafka.javaapi.consumer.ConsumerConnector javaConsumerConnector = Consumer.
                createJavaConsumerConnector(consumerConfig);
        String topic="access-log";
        Map<String,Integer> topicCountMap=new HashMap<>();
        topicCountMap.put(topic,1);
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = javaConsumerConnector.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> kafkaStreams = consumerMap.get(topic);
        for (KafkaStream stream:kafkaStreams){
            new Thread(new KafkaMessageProcessor(stream)).start();
        }


    }

    private class KafkaMessageProcessor implements Runnable{

        private KafkaStream<byte[], byte[]> kafkaStream;

        public KafkaMessageProcessor(KafkaStream kafkaStream) {
            this.kafkaStream = kafkaStream;
        }

        @Override
        public void run() {
            ConsumerIterator<byte[], byte[]> iterator = kafkaStream.iterator();
            while (iterator.hasNext()){

                String message = new String(iterator.next().message());
                try {
                    log.info("接收到kafka的消息并放入队列："+message);
                    queue.put(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Properties props=new Properties();
        props.put("zookeeper.connect","192.168.1.12:2181,192.168.1.13:2181,192.168.1.14:2181");
        props.put("group.id","eshop-cache-group");
        props.put("zookeeper.session.timeout.ms","40000");
        props.put("zookeeper.sync.time.ms","200");
        props.put("auto.commit.interval.ms","1000");
        ConsumerConfig consumerConfig=new ConsumerConfig(props);
        kafka.javaapi.consumer.ConsumerConnector javaConsumerConnector = Consumer.
                createJavaConsumerConnector(consumerConfig);
        String topic="access-log";
        Map<String,Integer> topicCountMap=new HashMap<>();
        topicCountMap.put(topic,1);
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = javaConsumerConnector.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> kafkaStreams = consumerMap.get(topic);
         BlockingQueue<String> queue=new ArrayBlockingQueue<>(1000);
        for (KafkaStream stream:kafkaStreams) {
            ConsumerIterator<byte[], byte[]> iterator = stream.iterator();
            while (iterator.hasNext()) {
                String message = new String(iterator.next().message());
                System.err.println(message);
                try {
                    queue.put(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
