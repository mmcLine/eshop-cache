package com.mmc.storm;

import com.mmc.storm.blot.LogParseBolt;
import com.mmc.storm.blot.ProductCountBolt;
import com.mmc.storm.spout.AccessLogKafkaSpout;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

/**
 * @description: 产品id热数据统计
 * @author: mmc
 * @create: 2019-10-24 23:21
 **/

public class HotProductTopology {

    public static void main(String[] args) throws InterruptedException {
        TopologyBuilder builder=new TopologyBuilder();
        builder.setSpout("AccessLogKafkaSpout",new AccessLogKafkaSpout(),1);
        builder.setBolt("LogParseBolt",new LogParseBolt(),2)
                .setNumTasks(2)
                .shuffleGrouping("AccessLogKafkaSpout");
        builder.setBolt("ProductCountBolt",new ProductCountBolt(),2)
                .setNumTasks(2)
                .fieldsGrouping("LogParseBolt",new Fields("productId"));
        Config config=new Config();
        if(args!=null&&args.length>0){
            config.setNumWorkers(3);
            try {
                StormSubmitter.submitTopology(args[0],config,builder.createTopology());
            } catch (AlreadyAliveException e) {
                e.printStackTrace();
            } catch (InvalidTopologyException e) {
                e.printStackTrace();
            } catch (AuthorizationException e) {
                e.printStackTrace();
            }
        }else{
            LocalCluster cluster=new LocalCluster();
            cluster.submitTopology("HotProductTopology",config,builder.createTopology());
            Thread.sleep(3000000);
            cluster.shutdown();
        }
    }
}
