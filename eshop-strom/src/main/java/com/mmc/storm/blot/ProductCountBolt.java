package com.mmc.storm.blot;

import com.alibaba.fastjson.JSON;
import com.mmc.common.constant.ServerConfig;
import com.mmc.common.constant.ZkNodeName;
import com.mmc.common.zk.ZooKeeperSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.storm.shade.org.apache.http.NameValuePair;
import org.apache.storm.shade.org.apache.http.client.utils.URLEncodedUtils;
import org.apache.storm.shade.org.apache.http.message.BasicNameValuePair;
import org.apache.storm.shade.org.apache.http.protocol.HTTP;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.trident.util.LRUMap;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.mmc.common.constant.ServerConfig.SERVER_URL1;
import static com.mmc.common.constant.ServerConfig.SERVER_URL2;

/**
 * @description: 产品id热点统计Bolt
 * @author: mmc
 * @create: 2019-10-24 23:18
 **/

public class ProductCountBolt extends BaseRichBolt {

    private LRUMap<Long,Long> productCountMap=new LRUMap<>(1000);

    private ZooKeeperSession zooKeeperSession;



    private int taskid;

    private Logger log=LoggerFactory.getLogger(ProductCountBolt.class);

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        taskid=topologyContext.getThisTaskId();
        zooKeeperSession=ZooKeeperSession.getInstance();
        new Thread(new ProductCountThread()).start();
        new Thread(new HotProductFindThread()).start();
        initTaskId(taskid);
    }

    private void initTaskId(int taskid){
        //ProductCountBolt所有的task启动的时候， 都会将自己的taskid写到同一个node的值中
        zooKeeperSession.acquireDistributedLock();
        try {
            String taskidList=zooKeeperSession.getNodeData(ZkNodeName.TASKID_LIST);

            if(StringUtils.isEmpty(taskidList)){
                taskidList+=taskid;
            }else {
                taskidList+=","+taskid;
            }
            zooKeeperSession.setNodeData(ZkNodeName.TASKID_LIST,taskidList);
        }finally {
            zooKeeperSession.releaseDistributedLock();
        }

    }

    @Override
    public void execute(Tuple tuple) {
        Long productId = tuple.getLongByField("productId");
        Long count = productCountMap.get(productId);
        if(count==null){
            productCountMap.put(productId,1L);
        }else {
            productCountMap.put(productId,++count);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
    private class ProductCountThread implements Runnable{

        @Override
        public void run() {
            List<Map.Entry<Long,Long>> topnProductList=new ArrayList<>();
            List<Long> topnIdList=new ArrayList<>();
            while (true){
                topnProductList.clear();
                topnIdList.clear();
                int topn=3;
                for (Map.Entry<Long,Long> entry:productCountMap.entrySet()){
                    if(topnProductList.size()==0){
                        topnProductList.add(entry);
                    }else{
                        boolean bigger = false;
                        for (int i=0;i<topnProductList.size();i++){
                            Map.Entry<Long,Long> topEntry=topnProductList.get(i);
                            if(entry.getValue()>topEntry.getValue()){
                                int lastIndex=topnProductList.size()<topn?topnProductList.size()-1:topn-2;

                                for (int j=lastIndex;j>=i;j--){
                                    if(j+1==topnProductList.size()){
                                        topnProductList.add(null);
                                    }
                                    topnProductList.set(j+1,topnProductList.get(j));
                                }
                                topnProductList.set(i,entry);
                                bigger=true;
                                break;
                            }
                        }
                        if(!bigger){
                            if(topnProductList.size()<topn){
                                topnProductList.add(entry);
                            }
                        }
                    }
                }
                try {
                    //获取到topnList
                    for (Map.Entry<Long,Long> entry:topnProductList){
                        topnIdList.add(entry.getKey());
                    }
                    String s = JSON.toJSONString(topnIdList);
                    log.info("热点商品id集合为："+topnIdList);
                    zooKeeperSession.setNodeData(ZkNodeName.HOT_PRODUCT_NODE+taskid,s);
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    private class HotProductFindThread implements Runnable{

        //倍数，超过平均值的10倍数即判定为热点数据
        private int multiple=10;
        List<Long> lastTimeHotList=new ArrayList<>();
        @Override
        public void run() {
            RestTemplate restTemplate=new RestTemplate();

                while (true){
                    try{
                    //1.排序
                    List<Map.Entry<Long,Long>> list=new ArrayList<>();
                    list.addAll(productCountMap.entrySet());
                    Collections.sort(list,(o1,o2)->Integer.valueOf(String.valueOf(o2.getValue()-o1.getValue())));
                    //2.计算后95%的平均值
                    int alculateCount= (int) Math.floor(list.size()*0.95);

                    Long totalCount=0L;
                    for (int i=list.size()-1;i>list.size()-alculateCount;i--){
                        totalCount+=list.get(i).getValue();
                    }
                    Long avgCount=alculateCount==0?1:totalCount/alculateCount;
                    avgCount=avgCount==0?1:avgCount;
                    log.info("平均值为："+avgCount);
                    // 3.从第一个元素开始遍历，判断是否是平均值得10倍
                    List<Long> hotList=new ArrayList<>();
                    for (Map.Entry<Long,Long> entry:list){
                        if(entry.getValue()>avgCount*multiple){
                            hotList.add(entry.getKey());
                            if(!lastTimeHotList.contains(entry.getKey())){
                                //将热点商品反向推给分发的nginx中
                                String distributeNginxURL=ServerConfig.SERVER_URL3+"hot?productId="+entry.getKey();
                                String result=restTemplate.getForObject(distributeNginxURL,String.class);

                                //将完整的缓存数据推给应用nginx
                                String cacheServiceURL = "http://localhost:8082/getProductInfo?productId=" + entry.getKey();
//                                ResponseEntity<ProductInfo> productInfoResponse = restTemplate.getForEntity(cacheServiceURL, ProductInfo.class);
//                                ProductInfo productInfo = productInfoResponse.getBody();
                                String productInfo = com.mmc.common.utils.HttpClientUtils.sendGetRequest(cacheServiceURL);
                                List<NameValuePair> params = new ArrayList<NameValuePair>();
                                params.add(new BasicNameValuePair("productInfo", productInfo));
                                String encode = URLEncodedUtils.format(params, HTTP.UTF_8);
                                String[] appNginxURLs = new String[]{
                                        SERVER_URL1+"hot?productId=" + entry.getKey() + "&" + encode,
                                        SERVER_URL2+"hot?productId=" + entry.getKey() + "&" + encode
                                };

                                for(String appNginxURL : appNginxURLs) {
                                    log.info("发送热点数据："+appNginxURL);
                                    restTemplate.getForObject(appNginxURL,String.class);
                                }
                            }

                        }
                    }

                    //如果热点数据消失，通知nginx
                    if(lastTimeHotList.size()==0){
                        lastTimeHotList.addAll(hotList);
                    }

                    for (Long id:lastTimeHotList){
                        if(!hotList.contains(id)){   //已消失的热点商品
                            String url=ServerConfig.SERVER_URL3+"cancel_hot?productId="+id;
                            log.info("取消热点数据："+url);
                            restTemplate.getForObject(url,String.class);
                        }
                    }
                    //更新上次热点数据
                    lastTimeHotList.clear();
                    lastTimeHotList.addAll(hotList);
                    Thread.sleep(5000);
                    }catch (Exception e2){
                        e2.printStackTrace();
                        System.exit(0);
                    }
            }



        }


    }

}
