import com.mmc.common.utils.HttpClientUtils;
import org.apache.storm.shade.org.apache.http.NameValuePair;
import org.apache.storm.shade.org.apache.http.client.utils.URLEncodedUtils;
import org.apache.storm.shade.org.apache.http.message.BasicNameValuePair;
import org.apache.storm.shade.org.apache.http.protocol.HTTP;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.mmc.common.constant.ServerConfig.SERVER_URL1;
import static com.mmc.common.constant.ServerConfig.SERVER_URL2;

/**
 * @description:
 * @author: mmc
 * @create: 2019-11-03 19:11
 **/

public class TestStorm {

    public static void main(String[] args) {
        String cacheServiceURL = "http://localhost:8082/getProductInfo?productId=" + 1;
//                                ResponseEntity<ProductInfo> productInfoResponse = restTemplate.getForEntity(cacheServiceURL, ProductInfo.class);
//                                ProductInfo productInfo = productInfoResponse.getBody();
        String productInfo = com.mmc.common.utils.HttpClientUtils.sendGetRequest(cacheServiceURL);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("productInfo", productInfo));
        String encode = URLEncodedUtils.format(params, HTTP.UTF_8);
        String[] appNginxURLs = new String[]{
                SERVER_URL1+"hot?productId=" + 1 + "&" + encode,
                SERVER_URL2+"hot?productId=" + 1 + "&" + encode
        };

        RestTemplate restTemplate =new RestTemplate();
        for(String appNginxURL : appNginxURLs) {
            System.out.println("发送请求："+appNginxURL);
            String s = HttpClientUtils.sendGetRequest(appNginxURL);
//            String forObject = restTemplate.getForObject(appNginxURL, String.class);
//            System.out.println(forObject);
        }
        System.out.println(1);
    }
}
