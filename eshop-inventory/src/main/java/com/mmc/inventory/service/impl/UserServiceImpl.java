package com.mmc.inventory.service.impl;

import com.alibaba.fastjson.JSON;
import com.mmc.inventory.bean.User;
import com.mmc.inventory.dao.RedisDao;
import com.mmc.inventory.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-28 23:13
 **/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private RedisDao redisDao;

    @Override
    public User getUserInfo() {
        return null;
    }

    @Override
    public User getCacheUserInfo() {
        redisDao.set("user_cache_1","{'id':1,'name':'小明'}");
        String cache1 = redisDao.get("user_cache_1");
        User user = JSON.parseObject(cache1, User.class);
        return user;
    }
}
