package com.mmc.inventory.service;

import com.mmc.inventory.bean.ProductInventory;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-30 22:23
 **/

public interface ProductInventoryService {

    /**
     * 更新商品库存
     * @param productInventory
     */
    void updateInventoryCnt(ProductInventory productInventory);

    /**
     * 查询商品库存
     * @param id
     * @return
     */
    ProductInventory findById(Integer id);


    /**
     * 删除商品库存缓存
     * @param id
     */
    void deleteProductInventoryCache(Integer id);


    /**
     * 设置缓存
     * @param productInventoryCache
     */
    void setProductInventoryCache(ProductInventory productInventoryCache);

    /**
     * 获取商品库存缓存
     * @param id
     * @return
     */
    ProductInventory getProductInventoryCache(Integer id);
}
