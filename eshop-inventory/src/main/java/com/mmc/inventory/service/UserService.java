package com.mmc.inventory.service;

import com.mmc.inventory.bean.User;

public interface UserService {

    User getUserInfo();

    User getCacheUserInfo();
}
