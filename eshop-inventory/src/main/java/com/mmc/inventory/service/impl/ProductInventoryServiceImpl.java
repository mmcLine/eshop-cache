package com.mmc.inventory.service.impl;

import com.mmc.inventory.bean.ProductInventory;
import com.mmc.inventory.biz.ProductInventoryBiz;
import com.mmc.common.constant.RedisPre;
import com.mmc.inventory.dao.RedisDao;
import com.mmc.inventory.service.ProductInventoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-30 22:29
 **/

@Service
public class ProductInventoryServiceImpl implements ProductInventoryService {

    @Autowired
    private ProductInventoryBiz productInventoryBiz;

    @Autowired
    private RedisDao redisDao;

    @Override
    public void updateInventoryCnt(ProductInventory productInventory) {
        productInventoryBiz.updateSelectiveById(productInventory);
    }

    @Override
    public ProductInventory findById(Integer id) {
        return productInventoryBiz.selectById(id);
    }

    @Override
    public void deleteProductInventoryCache(Integer id) {
        String key=RedisPre.getProductInventoryKey(id);
        redisDao.del(key);
    }

    @Override
    public void setProductInventoryCache(ProductInventory productInventoryCache) {
        String key=RedisPre.getProductInventoryKey(productInventoryCache.getId());
        redisDao.set(key,String.valueOf(productInventoryCache.getInventoryCnt()));
    }

    @Override
    public ProductInventory getProductInventoryCache(Integer id) {
        String key=RedisPre.getProductInventoryKey(id);
        String s = redisDao.get(key);
        if(StringUtils.isNotEmpty(s)){
            return ProductInventory.builder().id(id).inventoryCnt(Integer.valueOf(s)).build();
        }
        return null;
    }
}
