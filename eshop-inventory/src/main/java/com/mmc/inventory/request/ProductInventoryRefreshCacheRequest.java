package com.mmc.inventory.request;

import com.mmc.common.request.Request;
import com.mmc.inventory.bean.ProductInventory;
import com.mmc.inventory.service.ProductInventoryService;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-30 22:47
 **/

public class ProductInventoryRefreshCacheRequest implements Request {

    private Integer productId;

    private ProductInventoryService productInventoryService;

    //是否强制刷新
    private Boolean forceRefresh;

    public ProductInventoryRefreshCacheRequest(Integer productId, ProductInventoryService productInventoryService) {
        this.productId = productId;
        this.productInventoryService = productInventoryService;
        this.forceRefresh=false;
    }

    public ProductInventoryRefreshCacheRequest(Integer productId, ProductInventoryService productInventoryService, Boolean forceRefresh) {
        this.productId = productId;
        this.productInventoryService = productInventoryService;
        this.forceRefresh = forceRefresh;
    }

    @Override
    public void process() {
        //从数据库查询最新库存
        ProductInventory productInventoryDb = productInventoryService.findById(productId);
        //更新到缓存
        productInventoryService.setProductInventoryCache(productInventoryDb);

    }

    @Override
    public Integer id() {
        return productId;
    }

    public Boolean getForceRefresh() {
        return forceRefresh;
    }
}
