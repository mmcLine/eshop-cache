package com.mmc.inventory.request;

import com.mmc.common.request.Request;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description: 请求队列
 * @author: mmc
 * @create: 2019-09-29 23:28
 **/

public class RequestQueue {

    private List<BlockingQueue<Request>> requests=new ArrayList<>();

    private Map<Integer,Boolean> flagMap=new ConcurrentHashMap<>();

    private static class RequestQueueInstance{
        private static RequestQueue instance=new RequestQueue();
    }

    private RequestQueue(){}

    public static RequestQueue getInstance() {
        return RequestQueueInstance.instance;
    }

    public List<BlockingQueue<Request>> getRequests(){
        return requests;
    }

    public Integer getRequestQueueSize(){
        return requests.size();
    }

    public void add(BlockingQueue<Request> queue){
        requests.add(queue);
    }

    public BlockingQueue<Request> get(Integer index){
        return requests.get(index);
    }

    public Map<Integer, Boolean> getFlagMap() {
        return flagMap;
    }
}
