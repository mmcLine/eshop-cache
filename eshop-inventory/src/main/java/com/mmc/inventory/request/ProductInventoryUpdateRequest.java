package com.mmc.inventory.request;

import com.mmc.common.request.Request;
import com.mmc.inventory.bean.ProductInventory;
import com.mmc.inventory.service.ProductInventoryService;

/**
 * @description: 更新商品库存
 *
 * 修改库存
 * （1）删除缓存
 * （2）更新数据库
 *
 * @author: mmc
 * @create: 2019-09-30 21:53
 **/

public class ProductInventoryUpdateRequest implements Request {

    private ProductInventory productInventory;

    private ProductInventoryService productInventoryService;

    public ProductInventoryUpdateRequest(ProductInventory productInventory, ProductInventoryService productInventoryService) {
        this.productInventory = productInventory;
        this.productInventoryService = productInventoryService;
    }

    @Override
    public void process() {
        //删除缓存
        productInventoryService.deleteProductInventoryCache(productInventory.getId());
        //更新数据库
        productInventoryService.updateInventoryCnt(productInventory);
    }

    @Override
    public Integer id() {
        return productInventory.getId();
    }
}
