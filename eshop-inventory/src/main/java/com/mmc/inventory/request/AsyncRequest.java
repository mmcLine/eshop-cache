package com.mmc.inventory.request;

import com.mmc.common.request.Request;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * @description: 请求路由
 * @author: mmc
 * @create: 2019-10-01 10:29
 **/

@Service
public class AsyncRequest {


    public void add(Request request) throws InterruptedException {
        /**
         * 读请求去重操作逻辑
         */
        RequestQueue requestQueue=RequestQueue.getInstance();
        Map<Integer, Boolean> flagMap = requestQueue.getFlagMap();
        if(request instanceof ProductInventoryUpdateRequest){
            flagMap.put(request.id(),true);
        }else if(request instanceof ProductInventoryRefreshCacheRequest){
            ProductInventoryRefreshCacheRequest refreshCacheRequest=(ProductInventoryRefreshCacheRequest)request;
            if(!refreshCacheRequest.getForceRefresh()){
                Boolean flag=flagMap.get(request.id());
                //前面已有读请求，无需再加入了
                if(flag!=null&&!flag){
                    return;
                }
                flagMap.put(request.id(),false);
            }else {
                //强制刷新
                flagMap.put(request.id(),false);
            }

        }

        //根据id进行hash算法路由到对应的队列
        BlockingQueue<Request> routingQueue = getRoutingQueue(request.id());
        //放入对应的队列中
        routingQueue.put(request);
    }

    public BlockingQueue<Request> getRoutingQueue(Integer productId){
        RequestQueue requestQueue=RequestQueue.getInstance();
        String key=String.valueOf(productId);
        int h;
        int hash=(key==null)?0:(h=key.hashCode())^(h>>>16);

        //取模
        Integer index=(requestQueue.getRequestQueueSize()-1)&hash;
        return requestQueue.get(index);
    }
}
