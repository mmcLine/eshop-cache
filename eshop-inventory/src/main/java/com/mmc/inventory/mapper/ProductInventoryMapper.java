package com.mmc.inventory.mapper;

import com.mmc.inventory.bean.ProductInventory;
import tk.mybatis.mapper.common.Mapper;

public interface ProductInventoryMapper extends Mapper<ProductInventory> {
}
