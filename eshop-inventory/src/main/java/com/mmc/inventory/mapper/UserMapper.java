package com.mmc.inventory.mapper;

import com.mmc.inventory.bean.User;

import java.util.List;

public interface UserMapper {

    List<User> selectByid(Integer id);
}
