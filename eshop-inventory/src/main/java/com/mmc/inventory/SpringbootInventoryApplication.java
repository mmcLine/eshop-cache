package com.mmc.inventory;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.mmc.inventory.mapper")
public class SpringbootInventoryApplication {

	public static void main(String[] args) {
		SpringApplication app=new SpringApplication(SpringbootInventoryApplication.class);
		app.run(args);
	}

}
