package com.mmc.inventory.listener;

import com.mmc.inventory.thread.RequestProcessorThreadPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @description: 容器初始化监听器
 * @author: mmc
 * @create: 2019-09-29 23:10
 **/

public class InitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        RequestProcessorThreadPool instance = RequestProcessorThreadPool.getInstance();
    }
}
