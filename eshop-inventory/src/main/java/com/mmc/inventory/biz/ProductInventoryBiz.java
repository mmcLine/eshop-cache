package com.mmc.inventory.biz;

import com.mmc.common.biz.BaseBiz;
import com.mmc.inventory.bean.ProductInventory;
import com.mmc.inventory.mapper.ProductInventoryMapper;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-30 22:38
 **/

@Component
public class ProductInventoryBiz extends BaseBiz<ProductInventoryMapper,ProductInventory> {
}
