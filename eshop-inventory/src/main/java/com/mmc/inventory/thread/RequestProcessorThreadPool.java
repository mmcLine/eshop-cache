package com.mmc.inventory.thread;

import com.mmc.common.request.Request;
import com.mmc.inventory.request.RequestQueue;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description: 请求线程池
 * @author: mmc
 * @create: 2019-09-29 23:11
 **/
@Slf4j
public class RequestProcessorThreadPool {

    private static final int THREAD_NUM=10;

    private  ExecutorService executorService = Executors.newFixedThreadPool(THREAD_NUM);


    private RequestProcessorThreadPool(){
        log.info("线程池初始化......");
        for (int i=0;i<THREAD_NUM;i++){
            BlockingQueue<Request> queue=new ArrayBlockingQueue<Request>(100);
            RequestQueue instance = RequestQueue.getInstance();
            instance.getRequests().add(queue);
            executorService.submit(new RequestProcessorThread(queue));
        }
    }

    private static class RequestProcessorThreadPoolInstance{
        private static RequestProcessorThreadPool processorThreadPool=new RequestProcessorThreadPool();
    }

    public static RequestProcessorThreadPool getInstance(){
        return RequestProcessorThreadPoolInstance.processorThreadPool;
    }

}
