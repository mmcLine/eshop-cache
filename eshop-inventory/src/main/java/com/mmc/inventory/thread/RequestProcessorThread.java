package com.mmc.inventory.thread;

import com.mmc.common.request.Request;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

/**
 * @description: 执行请求的工作线程
 * @author: mmc
 * @create: 2019-09-29 23:30
 **/
@Slf4j
public class RequestProcessorThread implements Callable<Boolean> {

    private BlockingQueue<Request> queue;

    public RequestProcessorThread(BlockingQueue<Request> queue) {
        this.queue = queue;
    }

    @Override
    public Boolean call() {
        log.info("任务开始执行");
        while (true){
           try {
               Request request = queue.take();
               request.process();
           }catch (Exception e){
               e.printStackTrace();
           }
        }
    }
}
