package com.mmc.inventory.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @description:
 * @author: mmc
 * @create: 2019-08-28 21:59
 **/

@Controller
public class TestController {

//    private static int stock=50;



//    @Autowired
//    protected StringRedisTemplate redisTemplate;
//
//    @Autowired
//    private Redisson redisson;

    private static final Logger logger=LoggerFactory.getLogger(TestController.class);

//    @RequestMapping("test")
//    @ResponseBody
//    public String test(){
//
//        RLock redislock = redisson.getLock("redislock");
//        redislock.lock(10,TimeUnit.SECONDS);
//        try{
//            Integer stock=Integer.valueOf(redisTemplate.opsForValue().get("stock"));
//            if(stock>0){
//                stock--;
//                redisTemplate.opsForValue().set("stock",stock+"");
//                System.out.println("库存扣减成功："+Thread.currentThread().getName()+":  "+stock);
//
//                return "success";
//            }else {
//                System.out.println("库存为0，扣减失败");
//                return "error";
//            }
//        }finally {
//            redislock.unlock();
//        }
//
//    }

    @RequestMapping("testlog")
    public void testLog(){
        logger.info("哈哈哈哈哈哈哈哈哈哈");
    }


    public static void main(String[] args) throws IOException {
        FileWriter writer = new FileWriter("d://llj.txt", true);
        writer.append("\n");
        writer.write("abc");
        writer.close();
    }
}
