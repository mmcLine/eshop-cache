package com.mmc.inventory.controller;

import com.alibaba.fastjson.JSONArray;
import com.mmc.eacheapi.bean.ProductInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description:
 * @author: mmc
 * @create: 2019-11-04 23:50
 **/
@RestController
@Slf4j
public class ProductController {

    @RequestMapping("getProductInfo")
    public ProductInfo getProductInfo(Integer productId){
        ProductInfo productInfo=new ProductInfo();
        productInfo.setId(productId);
        productInfo.setName("牙膏");
        productInfo.setPrice(new BigDecimal("2.1"));
        productInfo.setUpdateTime(new Date());
        return productInfo;
    }


    @RequestMapping("getProductInfos")
    public String getProductInfos(String productIds){
        log.info("接收到请求："+productIds);
        JSONArray jsonArray=new JSONArray();
        for (String productId:productIds.split(",")){
            ProductInfo productInfo=new ProductInfo();
            productInfo.setId(Integer.valueOf(productId));
            productInfo.setName("纸巾");
            productInfo.setPrice(new BigDecimal("1.5"));
            productInfo.setUpdateTime(new Date());
            jsonArray.add(productInfo);
        }

        return jsonArray.toJSONString();
    }
}
