package com.mmc.inventory.controller;

import com.mmc.inventory.bean.ProductInventory;
import com.mmc.common.response.ObjectRestResponse;
import com.mmc.inventory.request.AsyncRequest;
import com.mmc.inventory.request.ProductInventoryRefreshCacheRequest;
import com.mmc.inventory.request.ProductInventoryUpdateRequest;
import com.mmc.inventory.service.ProductInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-01 10:46
 **/

@Controller
public class ProductInventoryController {

    @Autowired
    private AsyncRequest asyncRequest;

    @Autowired
    private ProductInventoryService productInventoryService;

    @RequestMapping("/updateProductInventory")
    @ResponseBody
    public void updateProductInventory(ProductInventory productInventory) throws InterruptedException {
        ProductInventoryUpdateRequest request=new ProductInventoryUpdateRequest(productInventory,productInventoryService);
        asyncRequest.add(request);
    }

    @RequestMapping("/refreshProductInventoryCache")
    @ResponseBody
    public ObjectRestResponse refreshProductInventoryCache(Integer productInventoryId) throws InterruptedException {
        ProductInventoryRefreshCacheRequest request=new ProductInventoryRefreshCacheRequest(productInventoryId,productInventoryService);
        asyncRequest.add(request);

        long startTime=System.currentTimeMillis();
        ProductInventory productInventory;
        //轮询从缓存中读取
        while (true){
            productInventory = productInventoryService.getProductInventoryCache(productInventoryId);
            if(productInventory!=null){
               break;
            }else {
                Thread.sleep(20);
            }

            //超过等待时长
            if(System.currentTimeMillis()-startTime>200){
                break;
            }
        }

        //还是为空，则从数据库查询
        if(productInventory==null){
            productInventory=productInventoryService.findById(productInventoryId);
            //避免redis因为内存满了被清空了，然后因为查询去重操作，缓存里永远没法设值
            ProductInventoryRefreshCacheRequest request2=new ProductInventoryRefreshCacheRequest(productInventoryId,productInventoryService,true);
            asyncRequest.add(request2);
        }
        return ObjectRestResponse.data(productInventory);
    }
}
