package com.mmc.inventory.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.Id;
import java.util.Date;

/**
 * @description: 商品库存
 * @author: mmc
 * @create: 2019-09-30 22:18
 **/

@Data
@Builder
public class ProductInventory {

    @Id
    private Integer id;
    //库存数量
    private Integer inventoryCnt;

    private Date createTime;

    @Tolerate
    public ProductInventory(){}
}
