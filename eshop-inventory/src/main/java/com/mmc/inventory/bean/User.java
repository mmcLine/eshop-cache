package com.mmc.inventory.bean;

import lombok.Data;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-28 21:46
 **/
@Data
public class User {

    private Integer id;
    private String name;
}
