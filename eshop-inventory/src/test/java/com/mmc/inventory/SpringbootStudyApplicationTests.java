package com.mmc.inventory;

import com.mmc.inventory.dao.RedisDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootStudyApplicationTests {

    @Autowired
    private RedisDao redisDao;

    @Test
    public void test(){
        redisDao.set("aaa","bbb");
        System.out.println(redisDao.get("aaa"));
    }

}
