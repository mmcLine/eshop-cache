package com.mmc.eache;

import com.mmc.common.zk.ZooKeeperSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-17 22:48
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestZookeeper {
    private String taskListPath = "/taskid-list-lock";
    @Test
    public void test(){
        ZooKeeperSession zookeeper = ZooKeeperSession.getInstance();
        zookeeper.create(taskListPath);
    }

}
