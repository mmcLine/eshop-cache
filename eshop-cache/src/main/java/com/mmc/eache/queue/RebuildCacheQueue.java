package com.mmc.eache.queue;

import com.mmc.eacheapi.bean.ProductInfo;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @description: 重建缓存
 * @author: mmc
 * @create: 2019-10-18 21:46
 **/

public class RebuildCacheQueue {

    private BlockingQueue<ProductInfo> blockingQueue=new ArrayBlockingQueue<>(1000);

    private static class Sington{

        private static RebuildCacheQueue rebuildCacheQueue=new RebuildCacheQueue();



        private static RebuildCacheQueue getInstance(){
            return rebuildCacheQueue;
        }
    }

    public static RebuildCacheQueue getInstance(){
        return Sington.getInstance();
    }

    public void addProduct(ProductInfo productInfo){
        try {
            blockingQueue.put(productInfo);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ProductInfo takeProduct(){
        ProductInfo productInfo = null;
        try {
            productInfo = blockingQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return productInfo;
    }




}
