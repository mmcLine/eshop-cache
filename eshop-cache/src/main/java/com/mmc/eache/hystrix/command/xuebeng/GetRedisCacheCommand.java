package com.mmc.eache.hystrix.command.xuebeng;

import com.alibaba.fastjson.JSON;
import com.mmc.common.constant.CommandKey;
import com.mmc.eache.dao.RedisDao;
import com.mmc.eache.util.SpringUtil;
import com.mmc.eacheapi.bean.ProductInfo;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixThreadPoolProperties;

/**
 * @description: 获取redis缓存
 * @author: mmc
 * @create: 2019-11-17 18:32
 **/

public class GetRedisCacheCommand extends HystrixCommand<ProductInfo> {

    private Integer productId;



    public GetRedisCacheCommand(Integer productId) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(CommandKey.REDIS_GROUP))
            .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                .withExecutionTimeoutInMilliseconds(1000)  //超时
                .withCircuitBreakerRequestVolumeThreshold(1000)    //滑动窗口中，有多少个qign'q请求，才会触发开启短路
                .withCircuitBreakerErrorThresholdPercentage(70)   //异常请求百分比达多少时，开启短路
                .withCircuitBreakerSleepWindowInMilliseconds(60*1000))   //短路之后，多长时间后尝试恢复
            //限流配置
            .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
                .withCoreSize(10)
                .withMaximumSize(30)
                .withAllowMaximumSizeToDivergeFromCoreSize(true)
                .withKeepAliveTimeMinutes(1)
                .withMaxQueueSize(50)
                .withQueueSizeRejectionThreshold(100))
        );
        this.productId = productId;
    }

    @Override
    protected ProductInfo run() throws Exception {
        String key="cache_product_"+productId;
        RedisDao redisDao= (RedisDao) SpringUtil.getBean("RedisDao");
        String s = redisDao.get(key);
        ProductInfo productInfo = JSON.parseObject(s, ProductInfo.class);
        return productInfo;
    }

    @Override
    protected ProductInfo getFallback() {
        return null;
    }
}
