package com.mmc.eache.hystrix.command;

import com.alibaba.fastjson.JSONObject;
import com.mmc.common.utils.HttpClientUtils;
import com.mmc.eacheapi.bean.ProductInfo;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixObservableCommand;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

import static com.mmc.common.constant.ServerConfig.INVENTORY_URL;

/**
 * @description: 批量查询商品的command
 * @author: mmc
 * @create: 2019-11-05 23:47
 **/

public class GetProductInfosCommand extends HystrixObservableCommand<ProductInfo> {

    private String[] productIds;

    public GetProductInfosCommand(String[] productIds){
        super(HystrixCommandGroupKey.Factory.asKey("GetProductInfoGroup"));
        this.productIds=productIds;
    }

    @Override
    protected Observable<ProductInfo> construct() {
        return Observable.create(new Observable.OnSubscribe<ProductInfo>() {
            @Override
            public void call(Subscriber<? super ProductInfo> subscriber) {
                try{
                    for (String productId:productIds){
                        String url = INVENTORY_URL+"getProductInfo?productId=" + productId;
                        String response = HttpClientUtils.sendGetRequest(url);
                        ProductInfo productInfo = JSONObject.parseObject(response, ProductInfo.class);
                        subscriber.onNext(productInfo);
                    }
                    subscriber.onCompleted();
                }catch (Exception e){
                    subscriber.onError(e);
                }

            }
        }).subscribeOn(Schedulers.io());
    }
}
