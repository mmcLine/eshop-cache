package com.mmc.eache.hystrix.command;

import com.netflix.hystrix.*;

/**
 * @description: 获取品牌名称
 * @author: mmc
 * @create: 2019-11-09 16:13
 **/

public class GetBrandNameCommand extends HystrixCommand<String> {

    private Long brandId;

    public GetBrandNameCommand(Long brandId){
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ProductInfoService"))
        .andCommandKey(HystrixCommandKey.Factory.asKey("GetProductInfoCommand"))
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("GetProductInfoPool"))
                .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
                .withCoreSize(15)
                .withQueueSizeRejectionThreshold(10))
        );
    }

    @Override
    protected String run() throws Exception {
        int a=10/0;
        return "iphone xs";
    }

    @Override
    protected String getFallback() {
        return "iphone";
    }
}
