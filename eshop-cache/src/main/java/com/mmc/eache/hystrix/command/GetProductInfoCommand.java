package com.mmc.eache.hystrix.command;

import com.alibaba.fastjson.JSONObject;
import com.mmc.common.utils.HttpClientUtils;
import com.mmc.eacheapi.bean.ProductInfo;
import com.netflix.hystrix.*;

import static com.mmc.common.constant.ServerConfig.INVENTORY_URL;

/**
 * @description: 查询商品数据的command
 * @author: mmc
 * @create: 2019-11-05 23:09
 **/

public class GetProductInfoCommand extends HystrixCommand<ProductInfo> {

    private Long productId;

    public GetProductInfoCommand(Long productId) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("GetProductInfoService"))
        .andCommandKey(HystrixCommandKey.Factory.asKey("GetProductInfoCommand"))
        .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("GetProductInfoPool"))
        .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
        .withCoreSize(10)
        .withMaximumSize(30)
        .withAllowMaximumSizeToDivergeFromCoreSize(true)    //允许自动扩容
        .withKeepAliveTimeMinutes(1)   //一定时间内线程少了，再缩容
        .withMaxQueueSize(8)
        .withQueueSizeRejectionThreshold(10))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                .withCircuitBreakerRequestVolumeThreshold(30)
                        .withCircuitBreakerErrorThresholdPercentage(40)
                        .withCircuitBreakerSleepWindowInMilliseconds(3000)
                        .withExecutionTimeoutInMilliseconds(1000)
                        .withFallbackIsolationSemaphoreMaxConcurrentRequests(30)
                )
        );
        this.productId=productId;
    }

    @Override
    protected ProductInfo run() throws Exception {
        String url = INVENTORY_URL+"getProductInfo?productId=" + productId;
        String response = HttpClientUtils.sendGetRequest(url);
        return JSONObject.parseObject(response, ProductInfo.class);
    }

    @Override
    protected String getCacheKey() {
        return "product_info_"+productId;
    }

    @Override
    protected ProductInfo getFallback() {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setId(productId.intValue());
        productInfo.setName("默认商品");
        return productInfo;
    }
}
