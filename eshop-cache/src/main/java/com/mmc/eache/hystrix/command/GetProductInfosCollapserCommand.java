package com.mmc.eache.hystrix.command;

import com.alibaba.fastjson.JSONArray;
import com.mmc.common.constant.ServerConfig;
import com.mmc.common.utils.HttpClientUtils;
import com.mmc.eacheapi.bean.ProductInfo;
import com.netflix.hystrix.HystrixCollapser;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;

import java.util.Collection;
import java.util.List;

/**
 * @description: 请求合并的获取商品
 * @author: mmc
 * @create: 2019-11-12 00:00
 **/

public class GetProductInfosCollapserCommand extends HystrixCollapser<List<ProductInfo>,ProductInfo,Long> {

    private Long productId;

    public GetProductInfosCollapserCommand(Long productId) {
        this.productId = productId;
    }

    @Override
    public Long getRequestArgument() {
        return productId;
    }

    @Override
    protected String getCacheKey() {
        return "product_info_"+productId;
    }

    @Override
    protected HystrixCommand<List<ProductInfo>> createCommand(Collection<CollapsedRequest<ProductInfo, Long>> collapsedRequests) {

        return new BatchCommand(collapsedRequests);
    }

    @Override
    protected void mapResponseToRequests(List<ProductInfo> batchResponse, Collection<CollapsedRequest<ProductInfo, Long>> collapsedRequests) {
        int count=0;
        for (CollapsedRequest request:collapsedRequests){
            request.setResponse(batchResponse.get(count++));
        }
    }


    private static final class BatchCommand extends HystrixCommand<List<ProductInfo>>{

        private final Collection<CollapsedRequest<ProductInfo,Long>> requests;

        public BatchCommand(Collection<CollapsedRequest<ProductInfo, Long>> requests) {
            super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ProductInfoService"))
            .andCommandKey(HystrixCommandKey.Factory.asKey("GetProductInfosCollapserBatchCommand")));
            this.requests = requests;
        }

        @Override
        protected List<ProductInfo> run() throws Exception {
            StringBuilder sb=new StringBuilder();
            for (CollapsedRequest request:requests){
                sb.append(request.getArgument()+",");
            }
            String products = sb.substring(0, sb.length()-1);
            String url=ServerConfig.INVENTORY_URL+"getProductInfos?productIds="+products;
            String result = HttpClientUtils.sendGetRequest(url);
            List<ProductInfo> productInfos = JSONArray.parseArray(result, ProductInfo.class);
            return productInfos;
        }
    }
}
