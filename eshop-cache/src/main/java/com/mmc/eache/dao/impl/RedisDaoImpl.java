package com.mmc.eache.dao.impl;

import com.mmc.eache.dao.RedisDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.JedisCluster;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-28 23:01
 **/
@Repository("RedisDao")
public class RedisDaoImpl implements RedisDao {

    @Autowired
    private JedisCluster jedisCluster;

    @Override
    public void set(String key, String value) {
        jedisCluster.set(key, value);
    }

    @Override
    public String get(String key) {
        return jedisCluster.get(key);
    }

    @Override
    public void del(String key) {
        jedisCluster.del(key);
    }
}
