package com.mmc.eache.dao;

/**
 * @description:
 * @author: mmc
 * @create: 2019-09-28 23:00
 **/

public interface RedisDao {

    void set(String key, String value);

    String get(String key);

    void del(String key);
}
