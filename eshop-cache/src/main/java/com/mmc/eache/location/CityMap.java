package com.mmc.eache.location;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: mmc
 * @create: 2019-11-07 21:42
 **/

public class CityMap {

    private static Map<Long,String> cityMap=new HashMap<>();

    static {
        cityMap.put(1L,"北京");
        cityMap.put(2L,"成都");
    }

    public static String getCityById(Long cityId){
        return cityMap.get(cityId);
    }
}
