package com.mmc.eache.kafka;

import com.alibaba.fastjson.JSON;
import com.mmc.common.constant.ZkNodeName;
import com.mmc.common.zk.ZooKeeperSession;
import com.mmc.eache.service.ProductInfoService;
import com.mmc.eacheapi.bean.ProductInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class KafkaReceiver {

    @Autowired
    private ProductInfoService productInfoService;

    @KafkaListener(topics = {"mmc"})
    public void listen(ConsumerRecord<?, ?> record) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            Object message = kafkaMessage.get();
                ProductInfo productInfo = (ProductInfo)JSON.parseObject(message.toString(), ProductInfo.class);
                log.info("record =" + record);
                //加分布式锁

                ZooKeeperSession zookeeperSession = ZooKeeperSession.getInstance();
                zookeeperSession.acquireFastFailDistributedLock(ZkNodeName.PRODUCT_INFO_ID+productInfo.getId());
                try {
                    ProductInfo redisCache = productInfoService.getRedisCache(productInfo.getId());
                    if(redisCache!=null){
                        //比较当前数据版本是新还是旧
                        if(redisCache.getUpdateTime().after(productInfo.getUpdateTime())){
                            return;
                        }
                    }

                    //假设生产环境阻塞
                    Thread.sleep(1000*5);
                    //更新本地缓存
                    productInfoService.saveLocalCache(productInfo);
                    productInfoService.saveRedisCache(productInfo);
                    log.info("缓存放入："+JSON.toJSONString(productInfo));
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    zookeeperSession.releaseDistributedLock(ZkNodeName.PRODUCT_INFO_ID+productInfo.getId());
                }
            }
    }
}
