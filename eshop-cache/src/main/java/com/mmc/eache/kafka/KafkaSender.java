package com.mmc.eache.kafka;

import com.alibaba.fastjson.JSON;
import com.mmc.eacheapi.bean.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Component
public class KafkaSender {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;


    public void send() {
        Message message = new Message();
        message.setId(System.currentTimeMillis());
        message.setMsg(UUID.randomUUID().toString());
        message.setSendTime(new Date());
        ProductInfo productInfo=new ProductInfo();
        productInfo.setId(15);
        productInfo.setName("矿泉水");
        productInfo.setPrice(new BigDecimal("2.1"));
        productInfo.setUpdateTime(new Date());
        kafkaTemplate.send("mmc", JSON.toJSONString(productInfo));
    }
}
