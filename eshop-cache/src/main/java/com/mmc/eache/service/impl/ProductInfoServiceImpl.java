package com.mmc.eache.service.impl;

import com.alibaba.fastjson.JSON;
import com.mmc.eache.dao.RedisDao;
import com.mmc.eache.service.ProductInfoService;
import com.mmc.eacheapi.bean.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-02 22:20
 **/
@Service("productInfoService")
public class ProductInfoServiceImpl implements ProductInfoService {

    private static final String CACHE_NAME="local";

    @Autowired
    private RedisDao redisDao;

    @Override
    @CachePut(value = CACHE_NAME,key = "'cache_product_'+#productInfo.getId()")
    public ProductInfo saveLocalCache(ProductInfo productInfo) {
        return productInfo ;
    }

    @Override
    @Cacheable(value = CACHE_NAME,key = "'cache_product_'+#id ")
    public ProductInfo getLocalCache(Integer id) {
        return null;
    }

    @Override
    public ProductInfo saveRedisCache(ProductInfo productInfo) {
        String key="cache_product_"+productInfo.getId();
        redisDao.set(key, JSON.toJSONString(productInfo));
        return productInfo;
    }

    @Override
    public ProductInfo getRedisCache(Integer id) {
        String key="cache_product_"+id;
        String s = redisDao.get(key);
        ProductInfo productInfo = JSON.parseObject(s, ProductInfo.class);
        return productInfo;
    }
}
