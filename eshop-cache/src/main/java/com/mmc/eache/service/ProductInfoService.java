package com.mmc.eache.service;

import com.mmc.eacheapi.bean.ProductInfo;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-02 22:15
 **/

public interface ProductInfoService {

    ProductInfo saveLocalCache(ProductInfo productInfo);

    ProductInfo getLocalCache(Integer id);

    ProductInfo saveRedisCache(ProductInfo productInfo);

    ProductInfo getRedisCache(Integer id);
}
