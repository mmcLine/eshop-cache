package com.mmc.eache.thread;

import com.mmc.common.zk.ZooKeeperSession;
import com.mmc.eache.queue.RebuildCacheQueue;
import com.mmc.eache.service.ProductInfoService;
import com.mmc.eache.util.SpringUtil;
import com.mmc.eacheapi.bean.ProductInfo;
import lombok.extern.slf4j.Slf4j;

import static com.mmc.common.constant.ZkNodeName.PRODUCT_INFO_ID;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-18 21:51
 **/
@Slf4j
public class RebuildCacheThread implements Runnable{


    @Override
    public void run() {
        log.info("重建缓存线程开始了.......");
        RebuildCacheQueue cacheQueue = RebuildCacheQueue.getInstance();
        ProductInfoService productInfoService = (ProductInfoService)SpringUtil.getBean("productInfoService");
        ZooKeeperSession zookeeperSession = ZooKeeperSession.getInstance();
        while (true){
            ProductInfo productInfo = cacheQueue.takeProduct();
            if(productInfo!=null){
                zookeeperSession.acquireFastFailDistributedLock(PRODUCT_INFO_ID+productInfo.getId());

                try{
                    ProductInfo redisCache = productInfoService.getRedisCache(productInfo.getId());
                    if(redisCache!=null){
                        //比较当前数据版本是新还是旧
                        if(redisCache.getUpdateTime().after(productInfo.getUpdateTime())){
                            continue;
                        }
                    }
                    //更新本地缓存
                    productInfoService.saveLocalCache(productInfo);
                    productInfoService.saveRedisCache(productInfo);
                    log.info("缓存重建："+productInfo);
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    zookeeperSession.releaseDistributedLock(PRODUCT_INFO_ID+productInfo.getId());
                }
            }
        }
    }
}
