package com.mmc.eache.listener;

import com.mmc.eache.thread.RebuildCacheThread;
import com.mmc.eache.util.SpringUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @description: 容器初始化监听器
 * @author: mmc
 * @create: 2019-09-29 23:10
 **/
@Component
public class InitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        SpringUtil.setApplicationContext(webApplicationContext);
        RebuildCacheThread rebuildCacheThread=new RebuildCacheThread();
        new Thread(rebuildCacheThread).start();
    }
}
