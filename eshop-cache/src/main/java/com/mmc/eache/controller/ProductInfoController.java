package com.mmc.eache.controller;

import com.mmc.common.response.ObjectRestResponse;
import com.mmc.eache.hystrix.command.xuebeng.GetRedisCacheCommand;
import com.mmc.eache.kafka.KafkaSender;
import com.mmc.eache.prewarm.CachePrewarmThread;
import com.mmc.eache.queue.RebuildCacheQueue;
import com.mmc.eache.service.ProductInfoService;
import com.mmc.eacheapi.bean.ProductInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-02 22:25
 **/
@Controller
@Slf4j
public class ProductInfoController {

    @Autowired
    private ProductInfoService productInfoService;

    @RequestMapping("saveProductCache")
    @ResponseBody
    public void saveProductCache(ProductInfo productInfo){
        productInfoService.saveLocalCache(productInfo);
    }

    @RequestMapping("loadProductCache")
    @ResponseBody
    public ObjectRestResponse loadProductCache(Integer id){
        ProductInfo localCache = productInfoService.getLocalCache(id);
        return ObjectRestResponse.data(localCache);
    }

    @Autowired
    private KafkaSender kafkaSender;


    @RequestMapping("testsend")
    @ResponseBody
    public ObjectRestResponse testsend(){
        kafkaSender.send();
        return ObjectRestResponse.success();
    }



    @RequestMapping("/getProductInfo")
    @ResponseBody
    public ProductInfo getProductInfo(Integer productId){
        if(productId==null){
            throw new IllegalArgumentException("productId is not null");
        }
//        ProductInfo productInfo = productInfoService.getRedisCache(productId);
        GetRedisCacheCommand cacheCommand=new GetRedisCacheCommand(productId);
        ProductInfo productInfo = cacheCommand.execute();
        if(productInfo==null){
            productInfo= productInfoService.getLocalCache(productId);
        }
        if(productInfo==null){
            //需要从数据源重新拉取，然后重建缓存
            //模拟从数据库查出的数据
            productInfo=getBydateBase(productId);
            RebuildCacheQueue.getInstance().addProduct(productInfo);
        }
        log.info("返回数据："+productInfo);
        return productInfo;
    }

    @RequestMapping("prewarmCache")
    public void prewarmCache(){
        new CachePrewarmThread().start();
    }

    private ProductInfo getBydateBase(Integer id){
        ProductInfo productInfo=new ProductInfo();
        productInfo.setId(id);
        //模拟的异常数据，处理缓存穿透
        if(id>5000){
            return productInfo;
        }
        productInfo.setPrice(new BigDecimal("2.5"));
        productInfo.setName("矿泉水");
        productInfo.setUpdateTime(new Date());
        return productInfo;
    }

}
