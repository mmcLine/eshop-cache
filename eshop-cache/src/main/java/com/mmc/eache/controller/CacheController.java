package com.mmc.eache.controller;

import com.mmc.common.utils.HttpClientUtils;
import com.mmc.eache.hystrix.command.GetProductInfoCommand;
import com.mmc.eache.hystrix.command.GetProductInfosCollapserCommand;
import com.mmc.eache.hystrix.command.GetProductInfosCommand;
import com.mmc.eacheapi.bean.ProductInfo;
import com.netflix.hystrix.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;
import rx.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @description:
 * @author: mmc
 * @create: 2019-11-04 23:42
 **/
@RestController
@Slf4j
@RequestMapping("/cache")
public class CacheController {


    @RequestMapping("/change/product")
    private String changeProduct(Long productId){
        String url="http://localhost:8081/getProductInfo?productId="+productId;
        String response = HttpClientUtils.sendGetRequest(url);
        log.info("获取到商品数据："+response);
        return "success";

    }

    @RequestMapping("/getProductInfo")
    public String getProductInfo(Long productId){
        HystrixCommand<ProductInfo> getInfoCommand=new GetProductInfoCommand(productId);
        ProductInfo productInfo = getInfoCommand.execute();

//        GetCityNameCommand getCityNameCommand = new GetCityNameCommand(productInfo.getCityId());
//        String cityName = getCityNameCommand.execute();
//        productInfo.setCityName(cityName);
//
//        GetBrandNameCommand getBrandNameCommand=new GetBrandNameCommand(productInfo.getBrandId());
//        String brandName = getBrandNameCommand.execute();
//        productInfo.setBrandName(brandName);

        log.info("查询到商品数据:"+productInfo);
        return "success";
    }

    @RequestMapping("/getProductInfoNoHystrix")
    public String getProductInfoNoHystrix(Long productId){
        String url = "http://localhost:8081/getProductInfo?productId=" + productId;
        String response = HttpClientUtils.sendGetRequest(url);
        log.info("查询到商品数据:"+response);
        return "success";
    }

    @RequestMapping("/getProductInfos")
    public String getProductInfos(String productIds){
        GetProductInfosCommand infosCommand = new GetProductInfosCommand(productIds.split(","));
        Observable<ProductInfo> observe = infosCommand.observe();
        observe.subscribe(new Observer<ProductInfo>() {
            @Override
            public void onCompleted() {
                log.info("获取完所有的商品");
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(ProductInfo productInfo) {
                log.info(productInfo.toString());
            }
        });
        return "success";
    }


    @RequestMapping("/getProductInfos2")
    public String getProductInfos2(String productIds){
        String[] productIdList = productIds.split(",");
        for (String productId:productIdList){
            GetProductInfoCommand productInfoCommand=new GetProductInfoCommand(Long.valueOf(productId));
            ProductInfo productInfo = productInfoCommand.execute();
            System.out.println(productInfoCommand.isResponseFromCache());
        }
        return "success";
    }

    @RequestMapping("/getProductInfoCollapser")
    public String getProductInfoCollapser(String productIds){
        List<Future<ProductInfo>> futures=new ArrayList<>();
        for (String productId:productIds.split(",")){
            GetProductInfosCollapserCommand getProductInfosCollapserCommand=new GetProductInfosCollapserCommand(Long.valueOf(productId));
            Future<ProductInfo> future = getProductInfosCollapserCommand.queue();
            futures.add(future);
        }
        for (Future future:futures){
            try {
                System.out.println(future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return "success";
    }

}
