package com.mmc.eache.prewarm;

import com.alibaba.fastjson.JSONArray;
import com.mmc.common.constant.ZkNodeName;
import com.mmc.common.zk.ZooKeeperSession;
import com.mmc.eache.service.ProductInfoService;
import com.mmc.eache.util.SpringUtil;
import com.mmc.eacheapi.bean.ProductInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description:
 * @author: mmc
 * @create: 2019-10-26 22:26
 **/
@Slf4j
public class CachePrewarmThread extends Thread {



    @Override
    public void run() {
        ProductInfoService productInfoService = (ProductInfoService)SpringUtil.getBean("productInfoService");
        ZooKeeperSession zooKeeperSession=ZooKeeperSession.getInstance();

        String taskidList = zooKeeperSession.getNodeData(ZkNodeName.TASKID_LIST);
        if(StringUtils.isNotEmpty(taskidList)){
            String[] taskidSplit = taskidList.split(",");
            for (String taskid:taskidSplit){
                String taskidLock=ZkNodeName.TASK_ID+taskid;

                boolean lock = zooKeeperSession.acquireFastFailDistributedLock(taskidLock);
                if(!lock){
                    continue;
                }
                String taskidStatusLock=ZkNodeName.TASKID_STATUS_LOCK+taskid;
                zooKeeperSession.acquireFastFailDistributedLock(taskidStatusLock);

                try {
                    String taskidStatus=zooKeeperSession.getNodeData(ZkNodeName.TASKID_STATUS+taskid);
                    //没有处理过该taskid
                    if(StringUtils.isEmpty(taskidStatus)){
                        String productidList=zooKeeperSession.getNodeData(ZkNodeName.HOT_PRODUCT_NODE+taskid);
                        JSONArray jsonArray = JSONArray.parseArray(productidList);
                        for (int i=0;i<jsonArray.size();i++){
                            Long productId=jsonArray.getLong(i);
                            //模拟数据
                            ProductInfo productInfo=new ProductInfo();
                            productInfo.setId(Integer.valueOf(productId+""));
                            productInfo.setName("手机");
                            productInfo.setPrice(new BigDecimal("1999"));
                            productInfo.setUpdateTime(new Date());
                            log.info("将商品保存到缓存中："+productInfo);
                            productInfoService.saveRedisCache(productInfo);
                            productInfoService.saveLocalCache(productInfo);
                        }
                        zooKeeperSession.setNodeData(ZkNodeName.TASKID_STATUS+taskid,"success");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    zooKeeperSession.releaseDistributedLock(taskidStatusLock);
                    zooKeeperSession.releaseDistributedLock(taskidLock);
                }

            }
        }

    }
}
